﻿using Grpc.Core;
using Grpc.Core.Interceptors;

namespace Anis.DemoGrpcProject.Interceptors
{
    public class InterceptorA : Interceptor
    {
        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
        {
            try
            {
                return await base.UnaryServerHandler(request, context, continuation);
            }
            catch (NullReferenceException e)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, e.Message));
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
    public class InterceptorB : Interceptor
    {
        public override Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
        {
            if (request is CreateRequest createRequest)
            {
                createRequest.Price = Math.Floor(Math.Pow(createRequest.Price, 2));
            }

            return base.UnaryServerHandler(request, context, continuation);
        }
    }
}
