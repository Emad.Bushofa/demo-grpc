﻿using FluentValidation;

namespace Anis.DemoGrpcProject.Validators
{
    public class CreateRequestValidator : AbstractValidator<CreateRequest>
    {
        public CreateRequestValidator()
        {
            RuleFor(x => x.Id).Must(id => Guid.TryParse(id, out _)).WithMessage("Invalid GUID");
            RuleFor(x => x.FirstName).NotEmpty().Length(3, 5);
            RuleFor(x => x.Age).GreaterThan(10);
            //RuleFor(x => x.Price).GreaterThan(0).LessThan(10); // 0.009999999 // 
        }
    }
}
