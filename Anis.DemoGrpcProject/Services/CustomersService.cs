using Grpc.Core;

namespace Anis.DemoGrpcProject.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        public override Task<CreateResponse> Create(CreateRequest request, ServerCallContext context)
        {
            return Task.FromResult(new CreateResponse
            {
                FullName = $"{request.FirstName} {request.Hhh} {request.LastName}",
                AgePlusYear = (int)request.Age
            });
        }
    }
}
