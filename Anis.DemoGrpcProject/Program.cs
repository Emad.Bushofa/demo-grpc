using Anis.DemoGrpcProject.Interceptors;
using Anis.DemoGrpcProject.Services;
using Anis.DemoGrpcProject.Validators;
using Calzolari.Grpc.AspNetCore.Validation;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddGrpc(options =>
{
    options.Interceptors.Add<InterceptorB>();
    options.EnableMessageValidation();
    options.Interceptors.Add<InterceptorA>();
});

builder.Services.AddValidator<CreateRequestValidator>();
builder.Services.AddGrpcValidation();

var app = builder.Build();


// Configure the HTTP request pipeline.
app.MapGrpcService<CustomersService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();


public partial class Program { }