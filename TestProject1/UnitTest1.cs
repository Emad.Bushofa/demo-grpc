using Calzolari.Grpc.Net.Client.Validation;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit.Abstractions;

namespace TestProject1
{
    public class UnitTest1 : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly WebApplicationFactory<Program> _factory;

        public UnitTest1(WebApplicationFactory<Program> factory, ITestOutputHelper helper)
        {
            _factory = factory.WithDefaultConfigurations(helper);
        }


        [Fact]
        public void Test1()
        {
            var client = new Customers.CustomersClient(_factory.CreateGrpcChannel());

            try
            {
                var response = client.Create(new CreateRequest
                {
                    Id = Guid.NewGuid().ToString(),
                    FirstName = "Ani",
                    MiddleName = "Ahmed",
                    LastName = "Khan",
                    CustomerAge = 11,
                    Price = 0.009999999
                });

                Console.WriteLine(response.FullName);
            }
            catch (RpcException e)
            {
                Assert.Equal(StatusCode.InvalidArgument, e.StatusCode);
                var errors = e.GetValidationErrors();
            }
        }
    }
}